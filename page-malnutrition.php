<?php get_template_part('templates/page', 'header'); ?>
<?php //get_template_part('templates/content', 'page'); ?>

<div id="ml-bb-wrapper">
    <div id="ml-bb-headings">
        <h3>Headings</h3>
        <div id="ml-bb-headings-links">
            <ul>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/malnutrition#mln-diagnosis')); ?>">How To Diagnose</a>
                    </h4>
                    <ul>
                        <li>
                            <a href="<?= esc_url(home_url('/malnutrition#mln-intro')); ?>">Introduction</a>
                        </li>
                        <li>
                            <a href="<?= esc_url(home_url('/malnutrition#mln-criteria')); ?>">Main Criteria</a>
                        </li>
                        <li>
                            <a href="<?= esc_url(home_url('/malnutrition#mln-signs')); ?>">Signs & Symptoms</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/malnutrition#mln-management')); ?>">Management</a>
                    </h4>
                    <ul>
                        <li>
                            <a href="<?= esc_url(home_url('/malnutrition#mln-mild')); ?>">Mild Malnutrition</a>
                        </li>
                        <li>
                            <a href="<?= esc_url(home_url('/malnutrition#mln-moderate')); ?>">Moderate Malnutrition</a>
                        </li>
                        <li>
                            <a href="<?= esc_url(home_url('/malnutrition#mln-severe')); ?>">Severe Malnutrition</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div id="ml-bb-body">
        <div class="ml-bb-section">
            <h3 id="mln-diagnosis">How to Diagnose</h3>
            <ul>
                <li>
                    <h4 id="mln-intro">Introduction</h4>
                    <p>
                        The diagnosis of malnutrition can be done using either the
                        <span class="mln-note">Mid Upper Arm Circumference (MUAC)</span>
                        or the <span class="mln-note">Z-Score</span> as main criteria,
                        with or without the <a href="<?= esc_url(home_url('/malnutrition#mln-signs')); ?>">
                        signs and symptoms</a> listed further below.
                    </p>
                </li>
                
                <li>
                    <h4 id="mln-criteria">Main Criteria</h4>
                    <h5>Mid Upper Arm Circumference (MUAC):</h5>
                    <p>
                        MUAC is measured using a tailor's tape. The tape is placed
                        midway between the elbow and the shoulder as shown below:<br>
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/malnutrition1.png"/>
                        <br>
                        <span class="mln-note">Normal:</span> > 12.5cm<br>
                        <span class="mln-note">Moderate Malnutrition:</span> 11.0 - 12.5cm<br>
                        <span class="mln-note">Severe Malnutrition:</span> < 11.0cm
                    </p>
                    <br>
                    <h5>Z-Score (Standard Deviation Score):</h5>
                    <p>
                        It is calculated using the child's most recent weight and
                        height, with the height measured using a tape.<br>
                        A Z-Score greater than +2 indicates <a href="<?= esc_url(home_url('/malnutrition#mln-severe')); ?>">
                        severe malnutrition</a>.<br>
                        A Z-Score between -2 and +2 indicates that your child is
                        not malnourished.<br>
                        A Z-Score between -3 and -2 indicates <a href="<?= esc_url(home_url('/malnutrition#mln-moderate')); ?>">
                        moderate malnutrition</a> (undernutrition to be more specific).<br>
                        A Z-Score of less than -3 indicates <a href="<?= esc_url(home_url('/malnutrition#mln-severe')); ?>">
                        severe malnutrition</a> (severe undernutrition to be more
                        specific).
                    </p>
                </li>
                <br>
                <li>
                    <h4 id="mln-signs">Signs & Symptoms</h4>
                    The following signs and symptoms would indicate that your child
                    is malnourished:<br>
                    <ul>
                        <li>
                            Pitting oedema on dorsum of foot (see images below).<br>
                            <img src="<?= bloginfo('stylesheet_directory'); ?>/assets/images/malnutrition3.png"/>                            
                            <img src="<?= bloginfo('stylesheet_directory'); ?>/assets/images/malnutrition2.png"/>                            
                        </li>
                        <li>
                            Ineffective feeding, that is, suckling difficulties observed
                            for 15 to 20 minutes in children less than 6 months of age.
                        </li>
                        <li>Moon (round) face.</li>
                        <li>Old man's face.</li>
                        <li>
                            Brown, scanty and brittle (easily broken) hair when compared
                            to that of other family members.
                        </li>
                        <li>
                            Loose and wrinkled skin (see image below):<br>
                            <img src="<?= bloginfo('stylesheet_directory'); ?>/assets/images/malnutrition4.png"/>
                        </li>
                        <li>Distended (large) abdomen.</li>
                        <li>Muscle wasting, especially in buttocks.</li>
                        <li>
                            Visible severe wasting (see image below):<br>
                            <img src="<?= bloginfo('stylesheet_directory'); ?>/assets/images/malnutrition5.png"/>
                        </li>
                        <li>
                            Hypo or hyper pigmentation of skin (loss of normal skin color).
                        </li>
                        <li>Diarrhoea</li>
                        <li>Loss of appetite</li>
                    </ul>
                </li>
            </ul>
        </div>
        
        <div class="ml-bb-section">
            <h3 id="mln-management">Management</h3>
            <ul>
                <li>
                    <h4 id="mln-mild">Mild Malnutrition</h4>
                    <p>
                        Encourage a balanced diet with protein-rich foods (e.g. beans,
                        groundnuts) and calorie-rich foods (e.g. rice, plantains,
                        cocoyams, etc).<br>
                        Also encourage intake of vegetables.
                    </p>
                </li>
                <li>
                    <h4 id="mln-moderate">Moderate Malnutrition</h4>
                    <p>
                        Supplementary feeding; This would be by using local foods
                        such as rice, beans and vegetables.<br>
                        Blended cereals such as corn-soya blend should also be given
                        to the child. (Not soya beans)
                    </p>
                </li>
                <li>
                    <h4 id="mln-severe">Severe Malnutrition</h4>
                    <h5>For children less than 6 months</h5>
                    <ul>
                        <li>
                            Increase frequency and duration of breastfeeding if baby
                            is breastfed.
                        </li>
                        <li>
                            Babies not suffering from oedema should be given supplementary
                            feeding using expressed breast milk. If that is not possible,
                            artificial milk (tin milk) or F75 should be given either
                            alone or together with expressed breast milk.<br>
                            Care of the malnourished child should continue at a health
                            centre, so <span class="mln-note">PLEASE SEE A DOCTOR</span>
                            as soon as possible.
                        </li>
                    </ul>
                    <br>
                    <h5>For children over 6 months</h5>
                    <p>
                        The management of their severe malnutrition is divided in
                        2 phases: the Stablization phase and the Rehabilitation phase.
                    </p>
                    <ol>
                        <li>
                            <span class="mln-note">Stabilization Phase</span><br>
                            <p class="mln-note">
                                It is worth noting that by the end of the second
                                day following the diagnosis, the child should have
                                been taken to the nearest health care facility for
                                proper medical care.
                            </p>
                            <p>
                                The stabilization is done using F75, a modified milk
                                feed diet to treat the immediate complications of
                                malnutrition.
                            </p>
                            <p class="mln-note">How to prepare F75 at home</p>
                            <ul>
                                <li>
                                    35g of milk (about 8 table spoons)
                                </li>
                                <li>
                                    70g of milk (about 16 cubes)
                                </li>
                                <li>
                                    20g of oil, Mayor or Azur (1 table spoon or 1
                                    tea spoon)
                                </li>
                                <li>
                                    35g of Bledina or Cerelac (about 8 table spoons)
                                </li>
                                <li>
                                    10ml of multivitamins or 1 sachet of Genesvit
                                    junior
                                </li>
                                <li>
                                    Mix the above components in 1l of clean water.
                                </li>
                            </ul>
                            <p class="mln-note">
                                NB: The F75 prepared as shown above should be stored
                                in a fridge and placed in warm water before each feed.
                            </p>
                            <br>
                            <p class="mln-note">
                                How to give F75 to the child
                            </p>
                            <table>
                                <tr>
                                    <th>Days</th>
                                    <th>Frequency (How often to feed)</th>
                                    <th>Quantity/kg per feed</th>
                                    <th>Quantity/kg per day</th>
                                </tr>
                                <tr>
                                    <td>1 - 2</td>
                                    <td>Two times each hour</td>
                                    <td>11ml</td>
                                    <td>130ml</td>
                                </tr>
                                <tr>
                                    <td>3 - 5</td>
                                    <td>Three times each hour</td>
                                    <td>16ml</td>
                                    <td>130ml</td>
                                </tr>
                                <tr>
                                    <td>6 onwards</td>
                                    <td>Four times each hour</td>
                                    <td>22ml</td>
                                    <td>130ml</td>
                                </tr>
                            </table>
                            <p class="mln-note">
                                *Quantity/kg = corresponding figure for day x weight
                                of child in kg.
                            </p>
                            <p>
                                Feed the child from a cup or bowl using a spoon,
                                or better still, a syringe (<span class="mln-note">without the needle)</span>
                            </p>
                            <p class="mln-note">Monitoring</p>
                            <ul>
                                <li>
                                    Always record amount of feed given and leftover.
                                </li>
                                <li>
                                    Take note if child vomits.
                                </li>
                                <li>
                                    Check for increased respiratory rate, that is,
                                    child breathes faster than normal. If it is the
                                    case, <span class="mln-note">you should see a
                                    doctor.</span>
                                </li>
                                <li>
                                    How many times does the child pass stool in a
                                    day? Are the stools soft, watery or normal?
                                </li>
                                <li>
                                    Check for signs of improvement such as:
                                    <ul>
                                        <li>
                                            Return of appetite
                                        </li>
                                        <li>
                                            Most or all of oedema (swelling) is gone.
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span class="mln-note">Rehabilitation Phase</span>
                            <p class="mln-note">
                                NB: The doctor will inform parents when the child
                                is ready to begin the rehabilitation phase.
                            </p>
                            <p>
                                The rehabilitation is done using diluted F100.
                            </p>
                            <p class="mln-note">
                                How to prepare diluted F100 at home:
                            </p>
                            <ul>
                                <li>
                                    110g of milk (about twenty four and a half table
                                    spoons)
                                </li>
                                <li>
                                    50g of sugar (about eleven and a half cubes)
                                </li>
                                <li>
                                    About 2 table spoons of oil, Mayor or Azur
                                </li>
                                <li>
                                    1 sachet of Genesvit junior
                                </li>
                                <li>
                                    Mix all the above in 1l of water, then add 300ml
                                    of water.
                                </li>
                            </ul>
                            <p class="mln-note">
                                NB: The F100 prepared as shown above should be stored
                                in a fridge and placed in warm water before each
                                feed.
                            </p>
                            <p class="mln-note">Other Aspects of Management</p>
                            <p>
                                Milk feeds aside (F75 and F100), the child also needs:
                            </p>
                            <ul>
                                <li>
                                    Lots of tenderness, love and care.
                                </li>
                                <li>
                                    A cheerful / happy environment.
                                </li>
                                <li>
                                    5 to 30 minutes of play time each day.
                                </li>
                            </ul>
                        </li>
                    </ol>
                </li>
            </ul>
            <br>
            <p class="mln-note">
                A Malnourished Child with Diarrhoea:
            </p>
            <ul class="mln-ul-bullets">
                <li>
                    If a malnourished child has diarrhoea, they are considered to
                    have some (moderate) dehydration.
                </li>
                <li>
                    This is treated using ReSoMal (Rehydration Solution for the
                    Malnourished) at a hospital or health centre.<span class="mln-note">
                    PLEASE SEE A DOCTOR!!</span>
                </li>
            </ul>
        </div>
    </div>
</div>
