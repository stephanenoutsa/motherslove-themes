<?php get_template_part('templates/page', 'header'); ?>
<?php //get_template_part('templates/content', 'page'); ?>

<div id="ml-bb-wrapper">
    <div id="ml-bb-headings">
        <h3>Headings</h3>
        <div id="ml-bb-headings-links">
            <ul>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/labour#preparing-for-labour')); ?>">Preparing For Labour</a>
                    </h4>
                </li>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/labour#signs-of-labour')); ?>">Signs Of Labour</a>
                    </h4>
                </li>
                <li><h4>
                        <a href="<?= esc_url(home_url('/labour#pain-management')); ?>">Coping With Labour Pain</a>
                    </h4>
                </li>                
            </ul>
        </div>
    </div>
    <div id="ml-bb-body">
        <div class="ml-bb-section">
            <h3 id="preparing-for-labour">Preparing For Labour</h3>
            <p>
                Every pregnant woman should have the following things ready by the
                seventh month (28th week) of pregnancy:
            </p>
            <ul>
                <li>Many clean napkins</li>
                <li>An antiseptic soap</li>
                <li>A packet of sterile gauze</li>
                <li>A packet of pad</li>
                <li>10 packets of sterile gloves</li>
                <li>2 sets of warm baby clothing, and clothing and underwear for the mother</li>
                <li>Alcohol</li>
                <li>A clean towel and other toiletries</li>
                <li>A clean bed sheet</li>
            </ul>
            <p>
                All these have to be well arranged in a box, and the box has to be
                closed and placed in an easily accessible corner of the house.
            </p>
        </div>
        
        <div class="ml-bb-section">
            <h3 id="signs-of-labour">Signs Of Labour</h3>
            
            <span class="ml-bb-xtic">Bloody Show</span>
            <p>
                As the cervix begins to get thin and dilate (open), a pink or bloody
                substance could be discharged.<br>
                You might also see a "plug" of mucus. This can happen several days
                before, or at the beginning of labour.
            </p>
            
            <span class="ml-bb-xtic">Ruptured Membranes</span>
            <p>
                This manifests as a gush or slow leak of amniotic fluid from the
                vagina. If this happens, you should immediately report the hospital.
            </p>
            
            <span class="ml-bb-xtic">Contractions</span>
            <p>
                When labour begins, the contractions feel like cramps. They become
                stronger, get closer together, and last longer.<br>
                Usually, first-time mothers go to the hospital when there is an interval
                of five minutes between contractions (for at least 1 - 2 hours),
                each lasting 45 - 60 seconds.<br>
                Second or third-time mothers go in when there is an interval of ten
                minutes between contractions which don't go away even when they rest.
            </p>
            
            <p class="ml-bb-note ml-bb-xtic">
                If you experience any of the above signs of labour, get your emergency
                box and consult at any health facility of your choice.
            </p>
        </div>
        
        <div class="ml-bb-section">
            <h3 id="pain-management">Coping With Labour Pain</h3>
            <p>
                There are many effective and helpful ways of reducing the amount of
                pain you feel when you have contractions. The effect varies from woman
                to woman, and often a combination of techniques are used to achieve
                this.<br>
                These techniques help you avoid medications, they are easy to do,
                and they give personal satisfaction.<br>
                Some of these techniques are:
            </p>
            <ul>
                <li>
                    Childbirth preparation classes. Learning about childbirth has
                    been proven to decrease anxiety. This is one of the activities
                    during ANC. Women mostly learn breathing exercises that decrease
                    labour pain.
                </li>
                <li>
                    Help from a supportive individual. Either a partner or close
                    relative / friend is very valuable to help with your comfort and
                    emotional support throughout the labour experience.
                </li>
                <li>
                    Being active during labour rather than staying in bed. Position
                    changes like walking, squatting, sitting, moving the hands and
                    knees, and using a birthing ball can all be helpful.
                </li>
                <li>
                    Pleasant surroundings - music, dim lights and aromatherapy.
                </li>
                <li>
                    Taking a warm water bath or shower may help reduce labour pain.
                </li>
            </ul>
            <p class="ml-bb-xtic">
                AVOID DRINKING TRADITIONAL CONCOCTIONS OR HONEY PRIOR TO / DURING
                LABOUR. THESE SUBSTANCES ARE OXYTOCIC AND THEREFORE INCREASE THE
                NUMBER OF CONTRACTIONS OCCURRING EACH MINUTE. THIS IS HARMFUL AS
                IT CAN LEAD TO THE RUPTURE OF THE UTERUS OR CAUSE FETAL DISTRESS.
            </p>
        </div>
    </div>
</div>
