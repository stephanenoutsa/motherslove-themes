<div id="ml-home-activity" class="container">
    <!-- Banner starts here -->
    <div id="ml-sliders">
        <div id="ml-slider-container" class="owl-carousel">
            <?php echo do_shortcode('[owl-carousel category="Home Sliders" autoplay="true" items="3"]');?>
            <div class="slide">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/image_anc.jpeg" alt=""/>
            </div>
            
            <div class="slide">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/image_malnutrition.jpeg" alt=""/>
            </div>
            
            <div class="slide">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/image_special_needs.jpeg" alt=""/>
            </div>
        </div>
    </div>
    <!-- Banner ends here -->
    <!-- Slider starts here -->
    <script>
//        $(document).ready(function(){
//            $(".owl-carousel").owlCarousel({
//                
//            });
//        });
    </script>
    <!-- Slider ends here -->
    
    <div id="ml-home-meta">
        
    </div>
</div>