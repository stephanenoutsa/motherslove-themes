<?php get_template_part('templates/page', 'header'); ?>
<?php //get_template_part('templates/content', 'page'); ?>

<div id="ml-contact-wrapper">
    <div id="ml-contact-entry" class="col-md-9">
        <?php
            echo do_shortcode('[contact-form-7 id="64" title="Contact form 1"]');
        ?>
    </div>

    <div id="ml-contact-info" class="col-md-3">
        <h5>Address</h5>
        <p>
            Mother's Love Initiative,<br>
            P.O. Box 0000,<br>
            Molyko - Buea,<br>
            Cameroon            
        </p>
        <p>
            Tel1: (+237) 679 386 277<br>
            Tel2: (+237) 674 447 555<br>
            Tel3: (+237) 676 768 586<br>
        </p>
        <p>
            Email
            <a href="mailto:contact@motherslove.com">contact@motherslove.com</a>
        </p>
    </div>
</div>
