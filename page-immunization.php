<?php get_template_part('templates/page', 'header'); ?>
<?php //get_template_part('templates/content', 'page'); ?>

<div id="ml-imm-wrapper" class="container">
    <div id="ml-imm-infants">
        <h4>Schedule For Infants</h4>
        <table>
            <tr>
                <th>CONTACT</th>
                <th>VACCINES (ADMINISTRATION SITE)</th>
            </tr>
            <tr>
                <th>At Birth</th>
                <td>BCG (Upper 1/3 of the front face of the left forearm)</td>
            </tr>
            <tr>
                <th>6 Weeks After Birth</th>
                <td>
                    OPV1 (In the mouth)<br>
                    DTC1-HEP B1-Hib1 (Left buttock)<br>
                    Pneumo13.1 (Right buttock)<br>
                    Rota1 (In the mouth)
                </td>
            </tr>
            <tr>
                <th>10 Weeks After Birth</th>
                <td>
                    OPV2 (In the mouth)<br>
                    DTC2-HEP B2-Hib2 (Left buttock)<br>
                    Pneumo13.2 (Right buttock)<br>
                    Rota2 (In the mouth)
                </td>
            </tr>
            <tr>
                <th>14 Weeks After Birth</th>
                <td>
                    OPV3 (In the mouth)<br>
                    DTC3-HEP B3-Hib3 (Left buttock)<br>
                    Pneumo13.3 (Right buttock)
                </td>
            </tr>
            <tr>
                <th>9 Months After Birth</th>
                <td>
                    Measles (Left deltoid)<br>
                    Yellow fever (Left buttock)<br>
                    Vitamin A (In the mouth)
                </td>
            </tr>
            <tr>
                <th>12 - 59 Months After Birth</th>
                <td>Vitamin A, 2 times per year (In the mouth)</td>
            </tr>
        </table>
    </div>
    <br>
    <div id="ml-imm-women">
        <h4>Schedule For Pregnant Women</h4>
        <table>
            <tr>
                <th>DOSE</th>
                <th>INTERVAL BETWEEN DOSES</th>
                <th>DURATION</th>
            </tr>
            <tr>
                <th>TTV1</th>
                <td>
                    1st contact or 1st prenatal visit
                </td>
                <td>0</td>
            </tr>
            <tr>
                <th>TTV2</th>
                <td>
                    4 weeks after TTV1
                </td>
                <td>1 - 3 years</td>
            </tr>
            <tr>
                <th>TTV3</th>
                <td>
                    6 months after TTV2
                </td>
                <td>5 years</td>
            </tr>
            <tr>
                <th>TTV4</th>
                <td>
                    1 year after TTV3
                </td>
                <td>10 years</td>
            </tr>
            <tr>
                <th>TTV5</th>
                <td>
                    1 year after TTV4
                </td>
                <td>Entire child bearing age range</td>
            </tr>
        </table>
        <p>
            <span class="ml-imm-note">NB:</span> "TTV" stands for "Tetanus Toxoid
            Vaccine".
        </p>
    </div>
</div>
