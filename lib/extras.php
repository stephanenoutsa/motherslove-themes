<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Set custom templates for display
 */
function ml_wrap_base_page($templates) {
    // Get the current post type
    $slug = get_the_slug();
    if ($slug) {
        // Shift the template to the front of the array
        array_unshift($templates, 'base-' . $slug . '.php');
    }
    // Return modified array with base-$cpt.php to the front of the queue
    return $templates;
}
// Add function to the sage_wrap_base filter
add_filter('sage/wrap_base', __NAMESPACE__ . '\\ml_wrap_base_page');

function get_the_slug() {
    global $post;
    if ( is_single() || is_page() ) {
        return $post->post_name;
    }
    else {
        return "";
    }
}
