<?php get_template_part('templates/page', 'header'); ?>
<?php //get_template_part('templates/content', 'page'); ?>

<div id="ml-about-wrapper">
    <div id="ml-about-text">
        <h3>What We Do</h3>
        <p>We try to reduce the infant and mortality rate in Cameroon.</p>
    </div>
    <div id="ml-team-wrapper">
        <h3>Team</h3>
        <div class="ml-team-member col-md-4">
            <p class="member-name">Patrick Ngan</p>
            <div class="member-picture">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/image_anc.jpeg" alt="Patrick Ngan" />
            </div>
            <div class="member-info">
                <p>
                    Patrick Ngan, founder of Mother's Love Initiative, is a 6th year student at the 
                    Faculty of Health Sciences, University of Buea.
                </p>
            </div>
        </div>
        <div class="ml-team-member col-md-4">
            <p class="member-name">Simo Alastair Mope</p>
            <div class="member-picture">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/image_anc.jpeg" alt="Simo Alastair Mope" />
            </div>
            <div class="member-info">
                <p>
                    Simo Alastair Mope, co-founder of Mother's Love Initiative, is a final year student
                    at the Faculty of Health Sciences, University of Buea.
                </p>
            </div>
        </div>
        <div class="ml-team-member col-md-4">
            <p class="member-name">Stéphane Noutsa</p>
            <div class="member-picture">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/image_anc.jpeg" alt="Stéphane Noutsa" />
            </div>
            <div class="member-info">
                <p>
                    Stéphane Noutsa, software developer for Mother's Love initiative, is a graduate student
                    from the Catholic University Institute of Buea.
                </p>
            </div>
        </div>
    </div>
</div>
