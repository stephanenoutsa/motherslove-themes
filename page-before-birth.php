<?php get_template_part('templates/page', 'header'); ?>
<?php //get_template_part('templates/content', 'page'); ?>

<div id="ml-bb-wrapper">
    <div id="ml-bb-headings">
        <h3>Headings</h3>
        <div id="ml-bb-headings-links">
            <ul>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/before-birth#anc-package')); ?>">Antenatal Clinic Package</a>
                    </h4>
                </li>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/before-birth#he-during-pregnancy')); ?>">Health Education During Pregnancy</a>
                    </h4>
                </li>
                <li><h4>
                        <a href="<?= esc_url(home_url('/before-birth#cc-during-pregnancy')); ?>">Common Complaints During Pregnancy</a>
                    </h4>
                </li>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/before-birth#high-risk-pregnancies')); ?>">High Risk Pregnancies</a>
                    </h4>
                </li>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/before-birth#ds-during-pregnancy')); ?>">Danger Signs During Pregnancy</a>
                    </h4>
                </li>
            </ul>
        </div>
    </div>
    <div id="ml-bb-body">
        <div class="ml-bb-section">
            <h3 id="anc-package">Antenatal Clinic Package</h3>
            <p>
                Antenatal Clinic (ANC) refers to the continual care given to mothers
                and their unborn babies throughout the pregnancy period.<br>
                ANC provides women and their families with appropriate information
                and advice for a healthy pregnancy, safe delivery, and recovery after
                delivery. It also includes care of the newborn, promotion of early,
                exclusive breastfeeding, and assistance with deciding on future pregnancies
                (family planning) in order to improve pregnancy outcomes.<br>
                An effective ANC package depends on competent health care providers
                in a well-equipped functioning health system, which can provide referral
                services and has laboratory support.<br>
                The World Health Organization (WHO) now recommends each pregnant
                woman to have at least FOUR focused antenatal care visits before delivery
                of the baby.<br>
                The goals and activities of ANC are summarized in each of the sections
                below:
            </p>
            <ul class="ml-bb-visits">
                <li>
                    <h4>First ANC Visit (8 - 12 weeks)</h4>
                    
                    <span class="ml-bb-xtic">Aim:</span>
                    <ul>
                        <li>Confirm pregnancy and calculate estimated date of delivery (EDD).</li>
                        <li>Classify women for basic ANC visits (four visits) or
                        more specialized care.</li>
                        <li>Screen, treat and give preventive measures for diseases.</li>
                        <li>Develop birth and emergency plans.</li>
                        <li>Advise and counsel pregnant women.</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Screening:</span>
                    <ul>
                        <li>Family/personal past history</li>
                        <li>Weight</li>
                        <li>Blood pressure</li>
                        <li>Anemia</li>
                        <li>Fundal height</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Tests:</span>
                    <ul>
                        <li>Haemoglobin</li>
                        <li>Blood/Rhesus group</li>
                        <li>Veneral disease screening</li>
                        <li>Hepatitis B & C / toxoplasmosis / rubella / HIV tests</li>
                        <li>Urine analysis</li>
                        <li>Stool analysis</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Treatment:</span>
                    <ul>
                        <li>Commence HIV treatment if positive.</li>
                        <li>Treat infections / diseases if indicated.</li>
                        <li>
                            Continue management of preexisting / de novo conditions
                            (e.g. diabetes, hypertension)
                        </li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Preventive measures for diseases:</span>
                    <ul>
                        <li>Tetanus toxoid</li>
                        <li>Iron and folate supplements</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Health education, advice and counseling:</span>
                    <ul>
                        <li>Self-care</li>
                        <li>Alcohol and tobacco use</li>
                        <li>Nutrition</li>
                        <li>Safe sex</li>
                        <li>Rest</li>
                        <li>Sleeping under ITN</li>
                        <li>Birth and emergency plan</li>
                    </ul>
                </li>
                <br>
                <li>                    
                    <h4>Second ANC Visit (22 - 24 weeks)</h4>
                    
                    <span class="ml-bb-xtic">Aim:</span>
                    <ul>
                        <li>Assess maternal and fetal well-being.</li>
                        <li>Exclude PIH and anemia.</li>
                        <li>Give preventive measures for diseases.</li>
                        <li>Review and modify birth and emergency plans.</li>
                        <li>Advise and counsel.</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Screening:</span>
                    <ul>
                        <li>Weight</li>
                        <li>Blood pressure</li>
                        <li>Anemia</li>
                        <li>Fundal height</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Tests:</span>
                    <ul>
                        <li>Urine analysis</li>
                        <li>Screen for diabetes mellitus</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Treatment:</span>
                    <ul>
                        <li>Commence HIV treatment if positive.</li>
                        <li>Treat infections / diseases if indicated.</li>
                        <li>
                            Continue management of preexisting / de novo conditions
                            (e.g. diabetes, hypertension)
                        </li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Preventive measures for diseases:</span>
                    <ul>
                        <li>Tetanus toxoid</li>
                        <li>Iron and folate supplements</li>
                        <li>IPTp</li>
                        <li>ARV</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Health education, advice and counseling:</span>
                    <ul>
                        <li>Birth and emergency plan.</li>
                        <li>Reinforcement of previous advice.</li>
                    </ul>
                </li>
                <br>
                <li>
                    <h4>Third ANC Visit (32 weeks)</h4>
                    
                    <span class="ml-bb-xtic">Aim:</span>
                    <ul>
                        <li>Assess maternal and fetal well-being.</li>
                        <li>Exclude PIH, anemia and multiple pregnancies.</li>
                        <li>Give preventive measures for diseases.</li>
                        <li>Review and modify birth and emergency plans.</li>
                        <li>Advise and counsel.</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Screening:</span>
                    <ul>
                        <li>Weight</li>
                        <li>Blood pressure</li>
                        <li>Anemia</li>
                        <li>Fundal height</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Tests:</span>
                    <ul>
                        <li>Urine analysis</li>
                        <li>Ultrasound</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Treatment:</span>
                    <ul>
                        <li>Commence HIV treatment if positive.</li>
                        <li>Treat infections / diseases if indicated.</li>
                        <li>
                            Continue management of preexisting / de novo conditions
                            (e.g. diabetes, hypertension)
                        </li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Preventive measures for diseases:</span>
                    <ul>
                        <li>Iron and folate supplements</li>
                        <li>IPTp</li>
                        <li>ARV</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Health education, advice and counseling:</span>
                    <ul>
                        <li>Birth and emergency plan.</li>
                        <li>Infant feeding.</li>
                        <li>Postpartum / postnatal care.</li>
                        <li>Pregnancy spacing.</li>
                        <li>Reinforcement of previous advice.</li>
                    </ul>
                </li>
                <br>
                <li>
                    <h4>Fourth ANC Visit (36 weeks)</h4>
                    
                    <span class="ml-bb-xtic">Aim:</span>
                    <ul>
                        <li>Assess maternal and fetal well-being.</li>
                        <li>Exclude PIH, anemia, multiple pregnancies and malpresentation.</li>
                        <li>Give preventive measures for diseases.</li>
                        <li>Review and modify birth and emergency plans.</li>
                        <li>Advise and counsel.</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Screening:</span>
                    <ul>
                        <li>Weight</li>
                        <li>Blood pressure</li>
                        <li>Anemia</li>
                        <li>Fundal height</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Tests:</span>
                    <ul>
                        <li>Urine analysis</li>
                        <li>Ultrasound</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Treatment:</span>
                    <ul>
                        <li>Commence HIV treatment if positive.</li>
                        <li>Treat infections / diseases if indicated.</li>
                        <li>
                            Continue management of preexisting / de novo conditions
                            (e.g. diabetes, hypertension)
                        </li>
                        <li>Rotation / referral for rotation of fetus if in breech.</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Preventive measures for diseases:</span>
                    <ul>
                        <li>Iron and folate supplements</li>
                        <li>ARV</li>
                    </ul>
                    <br>
                    <span class="ml-bb-xtic">Health education, advice and counseling:</span>
                    <ul>
                        <li>Birth and emergency plan.</li>
                        <li>Infant feeding.</li>
                        <li>Postpartum / postnatal care.</li>
                        <li>Pregnancy spacing.</li>
                        <li>Reinforcement of previous advice.</li>
                    </ul>
                </li>
            </ul>
        </div>
        
        <div class="ml-bb-section">
            <h3 id="he-during-pregnancy">Health Education During Pregnancy</h3>
            
            <span class="ml-bb-xtic">Adequate nutrition:</span>
            <p>
                Pregnant women are encouraged to eat foods rich in protein, vitamins,
                iron and calcium.
            </p>
            
            <span class="ml-bb-xtic">Clothing:</span>
            <p>
                Clothing should be free, light and hanging from shoulders. Avoid high
                heels, shoes with thin soles, belts and corsets.
            </p>
            
            <span class="ml-bb-xtic">Good hygiene:</span>
            <p>
                The mouth and breasts should be kept very clean throughout pregnancy
                and after delivery.
            </p>
            
            <span class="ml-bb-xtic">Sexual activity:</span>
            <p>
                Sexual intercourse should be avoided by pregnant women with threatened
                abortion, preterm labour or antepartum hemorrhage.
            </p>
            
            <span class="ml-bb-xtic">Travelling:</span>
            <p>
                It is allowed when comfortable. For patients with a history of antepartum
                hemorrhage or preterm labour, it is better to avoid travelling.
            </p>
            
            <span class="ml-bb-xtic">Weight gain:</span>
            <p>
                1 - 2 kg in the first trimester (first three months) of pregnancy,
                and an average weight gain of 0.4 kg/week from 20 weeks onwards.
            </p>
            
            <span class="ml-bb-xtic">Baths:</span>
            <p>
                Showers are preferable over tub baths. No vaginal douches are allowed.
            </p>
            
            <span class="ml-bb-xtic">Exercise:</span>
            <p>
                Exercises should be mild. Walking should be the preferred option.
                House work, if it is not tiring, is allowed.
            </p>
            
            <span class="ml-bb-xtic">Rest and sleep:</span>
            <p>
                Rest eight hours at night and two hours in the afternoon. Increase
                rest and sleep hours towards term.
            </p>
            
            <span class="ml-bb-xtic">Drugs:</span>
            <p>
                Avoid all unnecessary drugs during pregnancy. Minor complaints should
                be managed without the use of drugs whenever possible, by reassuring
                the patient.
            </p>
            
            <span class="ml-bb-xtic">Smoking and alcohol consumption:</span>
            <p>
                These should NOT be attempted during pregnancy given that they cause
                a lot of complications.
            </p>
            
            <span class="ml-bb-xtic">Irradiation:</span>
            <p>
                Avoid exposure to irradiation because of its teratogenic effect on
                the fetus.
            </p>
        </div>
        
        <div class="ml-bb-section">
            <h3 id="cc-during-pregnancy">Common Complaints During Pregnancy</h3>
            <ul>
                <li>Nausea and vomiting</li>
                <li>Heart burn</li>
                <li>Phyalism (excessive salivation)</li>
                <li>Constipation</li>
                <li>Hemorrhoids and varicose veins</li>
                <li>Swelling of feet</li>
                <li>Leg cramps</li>
                <li>
                    Excessive colorless, odorless vaginal discharge not associated
                    with burning sensations or itches.
                </li>
                <li>Back ache</li>
            </ul>
        </div>
        
        <div class="ml-bb-section">
            <h3 id="high-risk-pregnancies">High Risk Pregnancies</h3>
            <p>
                Certain pregnancies are classified as HIGH risk because they have
                a greater probability of jeopardizing maternal and fetal welfare.
                These conditions require a keener follow up during pregnancy. They
                include:            
            </p>
            <ul>
                <li>Early first pregnancy, less than 18 years old.</li>
                <li>Late first pregnancy, more than 35 years old.</li>
                <li>
                    Long duration of marriage with infertility and use of ovulatory
                    drugs.
                </li>
                <li>Women who have had 5 or more deliveries.</li>
                <li>Previous intrauterine death or neonatal death.</li>
                <li>Previous fetal malformation.</li>
                <li>
                    Previous second trimester (fourth to sixth months of pregnancy)
                    abortion or preterm labour.
                </li>
                <li>
                    Recurrent first trimester (first to third months of pregnancy)
                    abortions.
                </li>
                <li>Previous caesarian delivery.</li>
                <li>Past history of hypertension, heart disease or murmur.</li>
                <li>Chronic illnesses, e.g. cancer, diabetes.</li>
                <li>Previous surgery to uterus, including cervical cerclages.</li>
                <li>Maternal height less than 1.50m.</li>
            </ul>
            <p>
                Women who fall into any of these groups should inform their attending
                physicians about this so that special attention can be shown to them,
                or they could be referred to experienced physicians for follow up.
            </p>
        </div>
        
        <div class="ml-bb-section">
            <h3 id="ds-during-pregnancy">Danger Signs During Pregnancy</h3>
            <p>
                Pregnant women should seek IMMEDIATE care if they experience any of
                the following symptoms or signs:
            </p>
            <ul>
                <li>Vaginal bleeding, no matter how small it is.</li>
                <li>Severe swelling (of lower limbs, face or fingers).</li>
                <li>Spontaneous escape of fluid from the vagina.</li>
                <li>Abnormal gain or loss of weight.</li>
                <li>Decrease or cessation of fetal movements.</li>
                <li>Severe headache.</li>
                <li>Epigastric pain.</li>
                <li>Fever.</li>
                <li>Persistent vomiting.</li>
            </ul>
        </div>
    </div>
</div>
