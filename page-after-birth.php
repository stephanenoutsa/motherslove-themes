<?php get_template_part('templates/page', 'header'); ?>
<?php //get_template_part('templates/content', 'page'); ?>

<div id="ml-bb-wrapper">
    <div id="ml-bb-headings">
        <h3>Headings</h3>
        <div id="ml-bb-headings-links">
            <ul>
                <li><h4>
                        <a href="<?= esc_url(home_url('/after-birth#after-birth-intro')); ?>">Introduction</a>
                    </h4>
                </li>
                <li><h4>
                        <a href="<?= esc_url(home_url('/after-birth#changes-after-delivery')); ?>">Changes After Delivery</a>
                    </h4>
                </li>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/after-birth#frequent-complaints')); ?>">Frequent Complaints After Delivery</a>
                    </h4>
                </li>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/after-birth#he-after-delivery')); ?>">Health Education After Delivery</a>
                    </h4>
                </li>
            </ul>
        </div>
    </div>
    <div id="ml-bb-body">
        <div class="ml-bb-section">
            <h3 id="after-birth-intro">Introduction</h3>
            <p>
                The postpartum period (period after delivery) is the six week period
                after child birth, during which body tissues which have undergone
                changes during pregnancy return to their pre pregnancy state. It is
                also known as the puerperium. Women have to be aware of the course
                of these changes so as to reduce unnecessary fear and anxiety in
                them while the changes occur, and also so that they could quickly
                be aware if complications arise.
            </p>            
        </div>
        
        <div class="ml-bb-section">
            <h3 id="changes-after-delivery">Changes After Delivery</h3>
            
            <ol>
                <li>
                    <h4>BREASTFEEDING</h4>
                    <ul>
                        <li>
                            <h4>Importance of Breastfeeding</h5>
                            <p>
                                The establishment and maintenance of breastfeeding
                                should be one of the major goals of good postpartum
                                care. Human breast milk is the optimal food for newborn
                                infants. It is clean. When other foods are given,
                                especially by bottle feeding, it is very hard to keep
                                things clean enough to prevent the baby from getting
                                diarrhoea and other sicknesses. The temperature of
                                breast milk is always right.<br>
                                Not only is the breast milk nutritive, but it is also
                                immune protective as it contains antibodies which
                                protect the body against infections.<br>
                                Modified cow milk preparations of "formula" have
                                become readily available. These preparations are
                                close to human milk in nutrient quantity, but still
                                very different in quality, and lacking in immune
                                factors.<br>
                                In developing countries, artificial feeding is associated
                                with a much higher infant morbidity and mortality
                                than breastfeeding, primarily caused by infections
                                and malnutrition.
                            </p>
                        </li>
                        <br>
                        <li>
                            <h4>Initiation of Breastfeeding</h4>
                            <p>
                                It is recommended to give the baby to the mother to
                                hold immediately after delivery, to provide skin-to-skin
                                contact and for the baby to start suckling as soon
                                as he / she shows signs of readiness - normally within
                                30 minutes to 1 hour after birth. Early skin-to-skin
                                contact and suckling are associated with more affectionate
                                behavior of mothers towards their infants; mothers
                                who start to breastfeed early have fewer problems
                                with breastfeeding.
                            </p>
                        </li>
                        <br>
                        <li>
                            <h4>Positioning and Attaching the Baby to the Breast</h4>
                            <p>
                                If the baby is incorrectly attached, milk is not
                                effectively removed and the nipple may be damaged
                                by friction as the teat is drawn in and out of the
                                mouth. If the attachment is not corrected, sore nipples
                                and engorgement are more common developments, the
                                baby may get insufficient milk, and the mother is
                                more likely to stop breastfeeding.
                            </p>
                        </li>
                        <br>
                        <li>
                            <h4>The Need to Avoid Supplementary Feeds</h4>
                            <p>
                                In some hospitals it is common practice to give breastfed
                                babies supplements of formula or glucose water while
                                lactation is becoming established. This practice is
                                unnecessary because a healthy baby does not need extra
                                fluids or before breastfeeding is established, and
                                it is harmful because bottle feeding may interfere
                                with the initiation and continuation of breastfeeding.
                                Babies who have had their appetite satisfied with
                                an artificial feed may lose interest in trying to
                                breastfeed.<br>
                                WHO recommends that infants should be fed exclusively
                                on breast milk from birth to at least 4 and if possible
                                6 months of age. Breastfeeding should be done as
                                often as the child desires, day and night, at least
                                eight times in 24 hours. Often a sharp decline in
                                breastfeeding occurs in the weeks after delivery.
                            </p>
                        </li>
                        <br>
                        <li>
                            <h4>Consequences of Not Breastfeeding</h4>
                            <p>
                                If the baby is not put to the breasts early enough,
                                the breasts become engorged about the third to fourth
                                day after delivery because of the increased production
                                of milk at this time. This will cause discomfort to
                                the mother and can later predispose her to infections.<br>
                                This is also the reason why women who lose their
                                babies during labour are given medication to suppress
                                lactation.
                            </p>
                        </li>
                    </ul>
                </li>
                <br>
                <li>
                    <h4>After Pains</h4>
                    <p>
                        This refers to intermittent uterine contractions felt by
                        a breastfeeding mother in the region of the lower abdomen
                        in the course of breastfeeding her baby.<br>
                        This is normal and is as a result of physiological processes
                        occurring in the body during the time of breastfeeding. They
                        can be relieved by taking simple pain medication.
                    </p>
                </li>
                <br>
                <li>
                    <h4>Return to Normal Size of the Uterus</h4>
                    <p>
                        The uterus extends above the level of the umbilicus in most
                        women immediately after delivery. It should gradually reduce
                        daily after delivery with the result being that by the end
                        of the second week it should no longer be felt. This is a
                        natural process. As such, practices such as tying the stomach
                        tight or applying very hot water unto it are strongly discouraged.<br>
                        Failure of the uterus to reduce in size is not normal, especially
                        if it associated with fever. This should be reported to a
                        health personnel if noticed.
                    </p>
                </li>
                <br>
                <li>
                    <h4>Lochia</h4>
                    <p>
                        During these same 2 weeks, a woman should experience vaginal
                        discharges called lochia. For the first few days after delivery
                        (1 - 5 days) the lochia appears red. After 4 - 5 days it
                        becomes pale in color. After about the 10th day it becomes
                        yellowish-white in color.<br>
                        If lochia persists, or develops an offensive odor, or fails
                        to follow the aforementioned pattern of color change, it is
                        not normal and should be reported to a health personnel.
                    </p>
                </li>
                <br>
                <li>
                    <h4>Return of Menses</h4>
                    <p>
                        The time of return of the menses after a normal delivery is
                        quite varied. Many women who are not breastfeeding may resume
                        normal menses about 6 weeks after delivery. For mothers who
                        are breastfeeding, the picture is different. Those who introduce
                        supplemental feeding to breastfeeding quite early may have
                        their menses return before 6 weeks. The women who do exclusive
                        breastfeeding could have their menses delay for as long as
                        six months.<br>
                        However they should be aware that they could still get pregnant
                        despite this absence of menstruation.
                    </p>
                </li>
            </ol>
        </div>
        
        <div class="ml-bb-section">
            <h3 id="frequent-complaints">Frequent Complaints After Delivery</h3>
            <p>
                Women may experience problems following delivery. Occurrence of these
                problems varies from woman to woman. Women should report to their
                health care givers it they develop any of the following problems
                after delivery. They shall be provided with appropriate solutions:
            </p>
            <ul>
                <li>Fever</li>
                <li>Bladder problems (retention of urine)</li>
                <li>Backache</li>
                <li>Frequent headaches</li>
                <li>Pain at the genital region</li>
                <li>Hemorrhoids</li>
                <li>Constipation</li>
                <li>Depression, anxiety and extreme tiredness</li>
                <li>
                    Breast problems (engorgement, mastitis, cracked / sore / bleeding
                    nipples, inverted nipples)
                </li>
                <li>Anemia</li>
            </ul>
        </div>
        
        <div class="ml-bb-section">
            <h3 id="he-after-delivery">Health Education After Delivery</h3>
            <p>
                This is a very important part of management after delivery as women
                are taught ways of keeping themselves and their babies healthy.<br>
                Information is also provided for birth spacing in subsequent pregnancies.<br>
                Before leaving the health facility, each woman should receive education
                on the following:
            </p>
            <ul>
                <h4>Care of the Newborn</h4>
                <li>
                    Immediately after birth, mothers should ensure that their children
                    receive vitamin K injection to prevent them from developing hemorrhagic
                    disease of the newborn. Also, to prevent a newborn's eyes from
                    contracting dangerous conjunctivitis, mothers should ensure that
                    a little tetracycline eye ointment or 1% silver nitrate is dropped
                    in their baby's eyes after birth.
                </li>
                <li>
                    Protect the baby from cold, but also from too much heat. Dress
                    the baby as warmly as you feel like dressing yourself.
                </li>
                <br>
                <h4>Hygiene of the Baby</h4>
                <li>
                    Change the baby's diapers (nappy) each time he wets or dirties
                    them. If the skin gets sore, change the diaper more often, or
                    better still, leave it off.
                </li>
                <li>
                    Special care should be shown to keeping the umbilical cord clean.
                    Mothers should be taught how to wash their babies and how to
                    clean the umbilical region especially. After the cord drops off,
                    bathe the baby daily with soap and warm water.
                </li>
                <li>
                    If there are flies or mosquitoes, cover the baby's crib with
                    mosquito netting or a thin cloth.
                </li>
                <li>
                    Persons with open sores, colds, sore throats, tuberculosis or
                    other infectious illnesses should not touch or go near the baby.
                </li>
                <li>
                    Keep the baby in a clean place away from smoke and dust.
                </li>
                <br>
                <h4>Hygiene of the Mother</h4>
                <p>
                    Mothers should be encouraged to bathe at least twice a day, or
                    have at least 2 bed baths (for women who gave birth through caesarian
                    deliveries). The breast area should especially be kept clean.
                    Mothers should always wash their hands before feeding their babies.
                    Pads should be changed once they are wet, and women should complain
                    if they notice an offensive discharge.
                </p>
                <br>
                <h4>Vaccination</h4>
                <p>
                    Mothers should ensure that their babies receive the required
                    vaccines at birth and thereafter. Mothers should also endeavor
                    to receive all their tetanus toxoid vaccines as scheduled.<br>
                    (See <a href="<?= esc_url(home_url('/immunization')); ?>">"Immunization"</a>
                    for details.)
                </p>
                <br>
                <h4>Education on a Disease Which Can Be Transmitted from Mother to Child</h4>
                <p>
                    Before being discharged from the hospital, women shall be educated
                    on at least one disease which can be transmitted from mother to
                    child, or on prevention of certain pregnancy complications. Women
                    should be keen on reminding their doctors / nurses / midwives,
                    in case these forget.
                </p>
                <br>
                <h4>Family Planning</h4>
                <p>
                    Mothers will also be told when it would be appropriate for them
                    to plan their next pregnancy, if that is their desire. To achieve
                    this, they shall be introduced to various methods of contraception,
                    from which they will choose one they are comfortable with.<br>
                    Mothers who no longer desire pregnancy will also be proposed
                    methods they can use.<br>
                    The aim is to have adequate spacing among children, for a healthy
                    growth.
                </p>
                <br>
                <h4>RendezVous</h4>
                <p>
                    It is recommended that mothers report to the hospital with their
                    babies six weeks after delivery for checkup.
                </p>
            </ul>
        </div>
    </div>
</div>
