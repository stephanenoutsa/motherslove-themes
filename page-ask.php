<?php get_template_part('templates/page', 'header'); ?>
<?php //get_template_part('templates/content', 'page'); ?>

<?php
//    $titleError = $_POST['title-error'];
//    $questionError = $_POST['question-error'];
//    $tagsError = $_POST['tags-error'];
if(isset($_POST['submit'])) {
    // Check title
    if(trim($_POST['ml-ask-title']) === '') {
        $titleError = 'Please enter a title.';
        $hasError = true;
    }
    else {
        $title = trim($_POST['ml-ask-title']);
    }
    
    // Check question
    if(trim($_POST['ml-ask-body']) === '') {
        $questionError = 'Please enter a question.';
        $hasError = true;
    }
    else {
        $question = trim($_POST['ml-ask-question']);
    }
    
    // Check tags
    if (trim($_POST['ml-ask-tags']) === "ANC" || trim($_POST['ml-ask-tags']) === "anc" || trim($_POST['ml-ask-tags']) === "Malnutrition" || trim($_POST['ml-ask-tags']) === "malnutrition" || trim($_POST['ml-ask-tags']) === "Diarrhoea" || trim($_POST['ml-ask-tags']) === "diarrhoea" || trim($_POST['ml-ask-tags']) === "Special_Needs" || trim($_POST['ml-ask-tags']) === "special_needs") {
        $tags = trim($_POST['ml-ask-tags']);
    }
    else {
        $tagsError = 'Please check your tag.';
        $hasError = true;
    }
    
    if (!isset($hasError)) {
        header("Location: " . $home);
        exit();
    }
    
    else {
        $_POST['title-error'] = $titleError;
        $_POST['question-error'] = $questionError;
        $_POST['tags-error'] = $tagsError;
    }
}
?>

<div id="ml-ask-wrapper" class="container">
    <?php //echo "Title error is" . $titleError; ?>
    <form name="ml-ask-question" method="post" action="<?php /*bloginfo('stylesheet_directory'); ?>/form-validation.php*/the_permalink()?>">
        <label for="ml-ask-title">Title</label><br>
        <input type="text" id="ml-ask-title" class="ml-input" name="ml-ask-title"
               placeholder="Brief and specific title for your question"
               value="<?php if(isset($title)) echo $title; ?>"/>
        <?php if($titleError != '') { ?>
            <span class="error"><?php echo $titleError;?></span>
        <?php } ?>
        <label for="ml-ask-body">Question</label><br>
        <textarea id="ml-ask-body" class="ml-input" rows="10" name="ml-ask-body">
            <?php if(isset($question)) echo $question; ?>
        </textarea>
        <?php if($questionError != '') { ?>
            <span class="error"><?php echo$questionError;?></span>
        <?php } ?>
        <div class="ml-input-divider"></div><br>
        <div class="ml-input-divider"></div><br>
        <label for="ml-ask-tags">Tags</label><br>
        <input type="text" id="ml-ask-tags" class="ml-input" name="ml-ask-tags" 
               placeholder="anc, diarrhoea, malnutrition or special needs"/>
        <?php if($tagsError != '') { ?>
            <span class="error"><?php echo $tagsError;?></span>
        <?php } ?>
        <br><br>
        <input type="submit" id="ml-ask-submit" class="ml-input" name="ml-ask-submit" value="Post Your Question"/>
    </form>
</div>
