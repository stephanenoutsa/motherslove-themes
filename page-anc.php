<?php //get_template_part('templates/page', 'header'); ?>
<?php //get_template_part('templates/content', 'page'); ?>

<div id="ml-anc-wrapper" class="container">
    <div class="ml-block-left">
        <div class="col-md-12">
            <div class="ml-block-title">
                <a href="<?= esc_url(home_url('/before-birth')); ?>">
                    <h3>Before Birth</h3>
                </a>
            </div>
            <div class="ml-block-thumbnail">
                <a href="<?= esc_url(home_url('/before-birth')); ?>">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/image_anc.jpeg" alt="Before Birth" />
                </a>
            </div>
            <div class="ml-block-excerpt">
                <a href="<?= esc_url(home_url('/before-birth')); ?>">
                    <p>
                        Find out all you need to know about what to expect during pregnancy.
                    </p>
                </a>
            </div>
        </div>

        <div class="col-md-12">
            <div class="ml-block-title">
                <a href="<?= esc_url(home_url('/after-birth')); ?>">
                    <h3>After Birth</h3>
                </a>
            </div>
            <div class="ml-block-thumbnail">
                <a href="<?= esc_url(home_url('/after-birth')); ?>">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/image_anc.jpeg" alt="After Birth" />
                </a>            
            </div>
            <div class="ml-block-excerpt">
                <a href="<?= esc_url(home_url('/after-birth')); ?>">
                    <p>
                        Learn what changes could occur in your body after delivery of your child.
                    </p>
                </a>
            </div>
        </div>
    </div>
    
    <div class="ml-block-right">
        <div class="col-md-12">
            <div class="ml-block-title">
                <a href="<?= esc_url(home_url('/labour')); ?>">
                    <h3>Labour</h3>
                </a>
            </div>
            <div class="ml-block-thumbnail">
                <a href="<?= esc_url(home_url('/labour')); ?>">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/image_anc.jpeg" alt="Labour" />
                </a>
            </div>
            <div class="ml-block-excerpt">
                <a href="<?= esc_url(home_url('/labour')); ?>">
                    <p>
                        Be prepared for labour! Get all you need packed in your bag beforehand.
                    </p>
                </a>
            </div>
        </div>

        <div class="col-md-12">
            <div class="ml-block-title">
                <a href="<?= esc_url(home_url('/immunization')); ?>">
                    <h3>Immunization</h3>
                </a>
            </div>
            <div class="ml-block-thumbnail">
                <a href="<?= esc_url(home_url('/immunization')); ?>">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/image_anc.jpeg" alt="Before Birth" />
                </a>            
            </div>
            <div class="ml-block-excerpt">
                <a href="<?= esc_url(home_url('/immunization')); ?>">
                    <p>
                        Giving birth is great, but there's more to do to keep yourself and your child safe.
                    </p>
                </a>
            </div>
        </div>
    </div>
</div>
