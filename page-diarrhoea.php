<?php get_template_part('templates/page', 'header'); ?>
<?php //get_template_part('templates/content', 'page'); ?>

<div id="ml-bb-wrapper">
    <div id="ml-bb-headings">
        <h3>Headings</h3>
        <div id="ml-bb-headings-links">
            <ul>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/diarrhoea#dia-acute')); ?>">Acute Diarrhoea</a>
                    </h4>
                    <ul>
                        <li>
                            <a href="<?= esc_url(home_url('/diarrhoea#dia-acute-def')); ?>">Definition</a>
                        </li>
                        <li>
                            <a href="<?= esc_url(home_url('/diarrhoea#dia-acute-pres')); ?>">Presentation</a>
                        </li>
                        <li>
                            <a href="<?= esc_url(home_url('/diarrhoea#dia-acute-mgt')); ?>">Management</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/diarrhoea#dia-persist')); ?>">Persistent Diarrhoea</a>
                    </h4>
                    <ul>
                        <li>
                            <a href="<?= esc_url(home_url('/diarrhoea#dia-persist-def')); ?>">Definition</a>
                        </li>
                        <li>
                            <a href="<?= esc_url(home_url('/diarrhoea#dia-persist-types')); ?>">Types</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/diarrhoea#dia-dys')); ?>">Dysentery</a>
                    </h4>
                    <ul>
                        <li>
                            <a href="<?= esc_url(home_url('/diarrhoea#dia-dys-def')); ?>">Definition</a>
                        </li>
                        <li>
                            <a href="<?= esc_url(home_url('/diarrhoea#dia-dys-diagnosis')); ?>">Diagnosis</a>
                        </li>
                        <li>
                            <a href="<?= esc_url(home_url('/diarrhoea#dia-dys-treat')); ?>">Treatment</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div id="ml-bb-body">
        <div class="ml-bb-section">
            <h3 id="dia-acute">Acute Diarrhoea</h3>
            <ul>
                <li>
                    <h4 id="dia-acute-def">Definition</h4>
                    <p>
                        This is the passage of more than 3 loose stools in a day,
                        with no blood in the stools.<br>
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/diarrhoea.png"/>
                        <br>
                        <span class="mln-note">Picture:</span> Normal Stool Color
                    </p>
                </li>
                <br>
                <li>
                    <h4 id="dia-acute-pres">Presentation</h4>
                    <p>
                        All Children with diarrhoea should be assessed and classified
                        as either having severe dehydration, some (moderate) dehydration,
                        or no (mild) dehydration.
                    </p>
                    <br>
                    <h5>Severe Dehydration</h5>
                    <p>
                        Characterised by at least two of the following:
                    </p>
                    <ul>
                        <li>
                            Severe body weakness (lethargy) or unconsciousness
                        </li>
                        <li>Sunken eyes</li>
                        <li>Unable to drink or drinks poorly</li>
                        <li>
                            After pinching the skin, it returns to normal very slowly
                            (two seconds or more)
                        </li>
                    </ul>
                    <br>
                    <h5>Some (Moderate) Dehydration</h5>
                    <p>
                        Characterised by at least two of the following:
                    </p>
                    <ul>
                        <li>
                            Restlessness or irritability
                        </li>
                        <li>Sunken eyes</li>
                        <li>Drinks eageryly; thirstily</li>
                        <li>
                            After pinching the skin, it returns to normal slowly
                        </li>
                    </ul>
                    <br>
                    <h5>No (Mild) Dehydration</h5>
                    <p>
                        Not enough signs to classify as some dehydration or severe
                        dehydration.
                    </p>
                </li>
                <br>
                <li>
                    <h4 id="dia-acute-mgt">Management</h4>
                    <h5>No Dehydration (Mild)</h5>
                    <p class="mln-note">
                        Treatment is given at home.
                    </p>
                    <p class="mln-note">
                        How to prepare Oral Rehydration Solution (ORS) at Home:
                    </p>
                    <p>
                        In 1 litre of clean water, add 8 cubes of sugar + 1 teaspoon
                        of salt + 1 lemon (squeezed), then mix properly so that the
                        sugar and salt dissolve.
                    </p>
                    <p>
                        The treatment at home should follow the active rules listed
                        below:
                    </p>
                    <ul>
                        <li>Continue feeding</li>
                        <li>Give extra fluids</li>
                        <li>Give zinc supplements</li>
                        <li>When to go to the hospital or health centre</li>
                    </ul>
                    <ol>
                        <li>
                            <h5>Continue Feeding</h5>
                            <ul>
                                <li>
                                    If the child is breastfed, feed more frequently
                                    or longer.
                                </li>
                                <li>
                                    If exclusively breastfed, give ORS or clean water
                                    in addition to breast milk.
                                </li>
                            </ul>
                        </li>
                        <li>
                            <h5>Give Extra Fluids</h5>
                            <ul>
                                <li>
                                    If not exclusively breastfed, give either
                                    <span class="mln-note">ORS</span> or
                                    <span class="mln-note">CLEAN WATER</span>.
                                </li>
                                <li>
                                    To prevent dehydration, give as much fluid as
                                    the child will take, that is,
                                    <ul>
                                        <li>
                                            Less than 2 years: 50 - 100ml after every
                                            loose stool.
                                        </li>
                                        <li>
                                            Greater than 2 years: 100 - 200ml after
                                            every loose stool.
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    Give small sips from a cup. If the child vomits,
                                    wait 10 minutes then continue more slowly. Continue
                                    administering extra fluids till diarrhoea reduces.
                                </li>
                                <li>
                                    Mothers should have two packets of ORS or already
                                    locally prepared ORS at home.
                                </li>
                            </ul>
                        </li>
                        <li>
                            <h5>Give Zinc Supplements</h5>
                            <p>
                                Give zinc supplements as follows:
                            </p>
                            <ul>
                                <li>
                                    Less than 6 months: 1/2 tablet (10mg) per day
                                    for 10 - 14 days.
                                </li>
                                <li>
                                    Greater than 6 months: 1 tablet (20mg) per day
                                    for 10 - 14 days.
                                </li>
                            </ul>
                            <p>
                                Dissolve the zinc tablet in breast milk, clean water
                                or ORS before administering to the child.
                            </p>
                        </li>
                        <li>
                            <h5>When to go to the Hospital or Health Centre</h5>
                            <ul>
                                <li>
                                    If the child gets sicker, becomes unable to drink
                                    or breastfeed, develops a fever, or begins having
                                    blood in their stool, <span class="mln-note">take
                                    the child to a hospital or health centre as soon
                                    as possible</span>.
                                </li>
                                <li>
                                    If there is no improvement after 5 days,
                                    <span class="mln-note">take the child to a hospital
                                    or health centre.</span>
                                </li>
                            </ul>
                        </li>
                    </ol>
                    <br>
                    <p>
                        Administer the same treatment if the child suffers from similar
                        diarrhoea again.
                    </p>
                    <br>
                    <h5>Some Dehydration (Moderate)</h5>
                    <p class="mln-note">
                        Could be treated at home if the following steps are well
                        followed.
                    </p>
                    <p>
                        What do you use? Oral Rehydration Solution (ORS). This is
                        sold in packets at the hospital, health centre or pharmacy.
                        See above how to prepare ORS at home.<br>
                        <span class="mln-note">Below is a table showing what quantity of ORS to give to
                        the child. You should give the ORS with respect to the weight
                        of the child, and THE SOLUTION SHOULD BE GIVEN OVER A 4 HOUR
                        PERIOD.</span>
                    </p>
                    <table>
                        <tr>
                            <th>AGE</th>
                            <th>WEIGHT</th>
                            <th>AMOUNT OF ORS TO GIVE</th>
                        </tr>
                        <tr>
                            <td>< 4 months</td>
                            <td>< 6 kg</td>
                            <td>
                                200 - 400ml<br>
                                e.g. one smallest-sized plastic brasseries bottle
                                (350ml)
                            </td>
                        </tr>
                        <tr>
                            <td>4 - 12 months</td>
                            <td>6 to < 10kg</td>
                            <td>
                                400 - 700 ml<br>
                                e.g. two smallest-sized plastic brasseries bottles
                                (2 x 350ml)
                            </td>
                        </tr>
                        <tr>
                            <td>1 - 2 years</td>
                            <td>10 to < 12kg</td>
                            <td>
                                700 - 900ml<br>
                                e.g. 3/4 of a brasseries 1 litre plastic bottle (750ml)
                            </td>
                        </tr>
                        <tr>
                            <td>2 - 5 years</td>
                            <td>12 - 19 kg</td>
                            <td>
                                900 - 1400ml<br>
                                e.g. a brasseries 1 litre plastic bottle
                            </td>
                        </tr>
                    </table>
                    <ul>
                        <li>
                            If the child wants more ORS than is prescribed above,
                            feel free to give more.
                        </li>
                        <li>
                            Give frequent sips from a cup. If the child vomits, wait
                            10 minutes then continue, but more slowly.
                        </li>
                        <li>
                            Continue breastfeeding whenever the child wants.
                        </li>
                        <li>
                            Give zinc supplements as follows:
                            <ul>
                                <li>
                                    Less than 6 months: 1/2 tablet (10mg) per day
                                    for 10 - 14 days.
                                </li>
                                <li>
                                    Greater than 6 months: 1 tablet (20mg) per day
                                    for 10 - 14 days.<br>
                                    Dissolve the zinc tablet in breast milk, clean
                                    water or ORS before administering to the child.
                                </li>
                            </ul>
                        </li>
                        <li>
                            If the child gets sicker, becomes unable to drink or
                            breastfeed, develops a fever, or begins having blood
                            in their stool, <span class="mln-note">take the child
                            to a hospital or health centre as soon as possible.</span>
                        </li>
                        <li>
                            If there is no improvement after 5 days,
                            <span class="mln-note">take the child to a hospital or
                            health centre.</span>
                        </li>
                    </ul>
                    <p>
                        Administer the same treatment if the child suffers from similar
                        diarrhoea again.
                    </p>
                    <br>
                    <h5>Severe Dehydration</h5>
                    <p class="mln-note">
                        Should be treated at a hospital or health centre.
                    </p>
                    <p>
                        <span class="mln-note">Fluids to use:</span><br>
                        Isotonic fluids like Ringers lactate solution (Hartmanns
                        solution) and Normal Saline solution (0.9% NaCl).
                    </p>
                    <p>
                        <span class="mln-note">Do not use the following:</span><br>
                        5% glucose (dextrose) or 0.18 Saline with 5% dextrose solution.<br>
                        They increase the risk of hyponatraemia which can in turn
                        cause cerebral oedema.
                    </p>
                </li>
            </ul>
        </div>
        
        <div class="ml-bb-section">
            <h3 id="dia-persist">Persistent Diarrhoea</h3>
            <ul>
                <li>
                    <h4 id="dia-persist-def">Definition</h4>
                    <p>
                        Persistent diarrhoea is defined as diarrhoea with or without
                        blood in the stool which begins acutely, lasting 14 days
                        or more.
                    </p>
                </li>
                <li>
                    <h4 id="dia-persist-types">Types</h4>
                    <h5>Severe Persistent Diarrhoea</h5>
                    <ul>
                        <li>
                            <h5>Definition</h5>
                            <p>
                                This is diarrhoea lasting 14 days or more, with signs
                                of some dehydration or severe dehydration.
                            </p>
                        </li>
                        <li>
                            <h5>Treatment</h5>
                            <ul>
                                <li>
                                    <span class="mln-note">
                                        Take the child to the hospital.
                                    </span>
                                </li>
                                <li>
                                    Child should be assessed and treated according
                                    to Plan B (Some Dehydration) or Plan C (Severe
                                     Dehydration) depending on the dehydration.
                                </li>
                                <li>
                                    ORS is effective for most children with persistent
                                    diarrhoea. However, if the child does not tolerate
                                    ORS (i.e. there is an increase in stool volume,
                                    thirst or worsening of signs of dehydration),
                                    such children should be given treatment
                                    <span class="mln-note">at a hospital or health
                                    centre</span> until ORS can be tolerated without
                                    worsening of the diarrhoea.
                                </li>
                                <li>
                                    <span class="mln-note">Antibiotics treatment is
                                    NOT NECESSARY</span> unless there is evidence
                                    of an intestinal or non intestinal infection.
                                </li>
                            </ul>
                            <br>
                            <h5>Feeding</h5>
                            <span class="mln-note">Children less than 6 months:</span>
                            <ul>
                                <li>
                                    Breastfeeding should be continued for as often
                                    and long as the child wants.
                                </li>
                                <li>
                                    Other foods should not be given for as long as
                                    4 to 6 hours to allow for rehydration to be done,
                                    depending on the level of dehydration.
                                </li>
                            </ul>
                            <br>
                            <span class="mln-note">Children over 6 months:</span>
                            <ul>
                                <li>
                                    Restart feeding as soon as the child can eat.
                                    Feed the child at least 6 times a day to ensure
                                    they meet adequate energy requirements.
                                </li>
                            </ul>
                            <br>
                            <h5>Follow Up</h5>
                            <p>
                                Check the following to ensure that the child is improving:
                            </p>
                            <ul>
                                <li>
                                    Weight gain: Increase in weight for 3 consecutive
                                    days shows favourable response to treatment.
                                </li>
                                <li>
                                    Temperature: Absence of fever shows appropriate
                                    response to treatment.
                                </li>
                                <li>
                                    Number of stools per day: Passage of less than
                                    3 loose stools per day shows favourable response
                                    to treatment.
                                </li>
                                <li>
                                    Food taken in: Increase in quantity of food taken
                                    in by the child shows favourable response to
                                    treatment.
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <br>
                    <h5>Non-Severe Persistent Diarrhoea</h5>
                    <ul>
                        <li>
                            <h5>Definition</h5>
                            <p>
                                This is diarrhoea lasting 14 days or more with no
                                signs of dehydration and no severe malnutrition.
                            </p>
                        </li>
                        <li>
                            <h5>Treatment</h5>
                            <ul>
                                <li>
                                    Give treatment according to treatment Plan A
                                    (No or Mild Dehydration).
                                </li>
                                <li>
                                    ORS is effective for most children with persistent
                                    diarrhoea. However, if the child does not tolerate
                                    ORS (i.e. there is an increase in stool volume,
                                    thirst or worsening of signs of dehydration),
                                    such children should be given treatment
                                    <span class="mln-note">at a hospital or health
                                    centre</span> until ORS can be tolerated without
                                    worsening of the diarrhoea.
                                </li>
                                <li>
                                    <span class="mln-note">Antibiotics treatment is
                                    NOT NECESSARY</span> unless there is evidence
                                    of an intestinal or non intestinal infection.
                                </li>
                            </ul>
                            <br>
                            <h5>Feeding</h5>
                            <ul>
                                <li>
                                    Reduce the amount of milk in the child's diet
                                    temporarily.
                                </li>
                                <li>
                                    Continue breastfeeding more often and longer
                                    by day and night.
                                </li>
                                <li>
                                    If the child is taking some other form of milk,
                                    replace animal milk with fermented milk like
                                    yogurt which contains less lactose and is well
                                    tolerated.
                                </li>
                                <li>
                                    If the above mentioned replacement is not possible,
                                    reduce animal milk to a maximum  of 50ml/kg (i.e.
                                    50 x child's weight in kg) in 24 hours.
                                </li>
                                <li>
                                    Formula-fed children that are older than 4 months
                                    should start taking in solid foods.
                                </li>
                                <li>
                                    Give frequent small feeds, at least 6 times per
                                    day.
                                </li>
                            </ul>
                            <br>
                            <h5>Follow Up</h5>
                            <ul>
                                <li>
                                    Take the child back to the hospital or health
                                    centre after 5 days at most if the  diarrhoea
                                    worsens or other problems develop.
                                </li>
                                <li>
                                    Check for signs of dehydration and the child's
                                    body temperature to ensure there is no fever.
                                </li>
                                <li>
                                    If the child has gained weight and has less than
                                    3 loose stools per day, they may resume the normal
                                    diet for their age.
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        
        <div class="ml-bb-section">
            <h3 id="dia-dys">Dysentery</h3>
            <ul>
                <li>
                    <h4 id="dia-dys-def">Definition</h4>
                    <p>
                        Dysentery is defined as loose stools containing blood.<br>
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/dysentery.png"/>
                        <br>
                        <span class="mln-note">Picture:</span> Loose Stool Containing
                        Blood
                    </p>
                </li>
                <li>
                    <h4 id="dia-dys-diagnosis">Diagnosis</h4>
                    <p>
                        A child with dysentery will have some or all of the following
                        signs:
                    </p>
                    <ul>
                        <li>Abdominal pain</li>
                        <li>Fever</li>
                        <li>Severy body weakness</li>
                        <li>Dehydration</li>
                    </ul>
                </li>
                <li>
                    <h4 id="dia-dys-treat">Treatment</h4>
                    <p class="mln-note">
                        Take the child to a hospital or health centre as soon as
                        possible.
                    </p>
                    <h5>Follow Up</h5>
                    <p>
                        Two days after starting the treatment at the hospital or
                        health centre, check for signs of improvement such as:
                    </p>
                    <ul>
                        <li>No fever</li>
                        <li>Fewer stools with less blood</li>
                        <li>Improved appetite</li>
                    </ul>
                    <p>
                        If there is no improvement, return to the hospital.
                    </p>
                    <p>
                        Treat for dehydration depending on the level of dehydration.
                    </p>
                    <h5>Feeding</h5>
                    <span class="mln-note">Less than 6 months:</span>
                    <br>
                    Continue breastfeeding throughout the illness, but do so more
                    frequently than usual.
                    <br>
                    <span class="mln-note">Over 6 months</span>
                    <ul>
                        <li>
                            They should receive their normal meals.
                        </li>
                        <li>
                            Encourage the child to eat, and allow the child to select
                            preferred foods.
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
