<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$home = esc_url(home_url());
$ask = esc_url(home_url('/ask'));

if(isset($_POST['submit'])) {
    // Check title
    if(trim($_POST['ml-ask-title']) === '') {
        $titleError = 'Please enter a title.';
        $hasError = true;
    }
    else {
        $title = trim($_POST['ml-ask-title']);
    }
    
    // Check question
    if(trim($_POST['ml-ask-body']) === '') {
        $questionError = 'Please enter a question.';
        $hasError = true;
    }
    else {
        $question = trim($_POST['ml-ask-question']);
    }
    
    // Check tags
    if (trim($_POST['ml-ask-tags']) === "ANC" || trim($_POST['ml-ask-tags']) === "anc" || trim($_POST['ml-ask-tags']) === "Malnutrition" || trim($_POST['ml-ask-tags']) === "malnutrition" || trim($_POST['ml-ask-tags']) === "Diarrhoea" || trim($_POST['ml-ask-tags']) === "diarrhoea" || trim($_POST['ml-ask-tags']) === "Special_Needs" || trim($_POST['ml-ask-tags']) === "special_needs") {
        $tags = trim($_POST['ml-ask-tags']);
    }
    else {
        $tagsError = 'Please check your tag.';
        $hasError = true;
    }
    
    if (!isset($hasError)) {
        header("Location: " . $home);
        exit();
    }
    
    else {
        $_POST['title-error'] = $titleError;
        $_POST['question-error'] = $questionError;
        $_POST['tags-error'] = $tagsError;
    }
}

?>
