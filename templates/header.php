<header id="ml-main-header" class="banner navbar navbar-default navbar-static-top" role="banner">
    <div class="container">
    
        <div class="ml-header-top">
            <div class="ml-logo-wrapper">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo.png">
                    </a>
    <!--                <br>
                    <a class="brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>-->
                </div>
            </div>

            <div class="ml-account">
                <?php
                if (is_user_logged_in()) {?>
                <div class="ml-user-avatar">

                </div>
                <div class="ml-user-info">
                    <select class="dropdown">
                        <?php $current_user = wp_get_current_user(); ?>
                        <option value="" class="label"><?php echo $current_user->display_name; ?></option>
                        <option value="1"><a href="<?php bp_get_displayed_user_link()?>">Account</a></option>
                    <option value="2"><a href="<?php wp_logout()?>">Log out</a></option>
                    </select>
                </div>
                <?php }
                else {?>
                <a href="<?php echo wp_registration_url(); ?>">Sign Up</a> <span class="ml-separate">|</span>
                <a href="<?php echo wp_login_url(); ?>">Login</a>
                <?php } ?>
            </div>
        </div>        
        
        <div class="ml-nav-wrapper">
            <nav class="collapse navbar-collapse" role="navigation">
                <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav']);
                endif;
                ?>
            </nav>  
        </div>
        
    </div>
</header>
