<footer class="content-info">
    <div class="container">
        <?php //dynamic_sidebar('sidebar-footer'); ?>
        <div class="footer-rights">
            <p>Copyright &COPY; 2016 | <span>Mother's Love Initiative</span></p>
        </div>
        <div class="footer-designer">
            <p>Designed by Stéphane Noutsa</p>
        </div>
    </div>
</footer>
