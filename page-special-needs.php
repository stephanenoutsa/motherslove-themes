<?php get_template_part('templates/page', 'header'); ?>
<?php //get_template_part('templates/content', 'page'); ?>

<div id="ml-bb-wrapper">
    <div id="ml-bb-headings">
        <h3>Headings</h3>
        <div id="ml-bb-headings-links">
            <ul>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/special-needs#hiv')); ?>">HIV & Pregnancy</a>
                    </h4>
                    <ul>
                        <li>
                            <a href="<?= esc_url(home_url('/special-needs#hiv-intro')); ?>">Introduction</a>
                        </li>
                        <li>
                            <a href="<?= esc_url(home_url('/special-needs#hiv-approach')); ?>">Approach</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <h4>
                        <a href="<?= esc_url(home_url('/special-needs#hep')); ?>">Hepatitis & Pregnancy</a>
                    </h4>
                    <ul>
                        <li>
                            <a href="<?= esc_url(home_url('/special-needs#hep-intro')); ?>">Introduction</a>
                        </li>
                        <li>
                            <a href="<?= esc_url(home_url('/special-needs#hep-approach')); ?>">Approach</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div id="ml-bb-body">
        <div class="ml-bb-section">
            <h3 id="hiv">HIV & Pregnancy</h3>
            <ul>
                <li>
                    <h4 id="hiv-intro">Introduction</h4>
                    <ul>
                        <li>
                            This section focuses on the diagnosis and prevention
                            of mother to child transmission (PMTCT) of HIV as 90%
                            of children infected with HIV get it from their mothers.
                        </li>
                        <li>
                            In 2012, there were still an estimated 260,000 new infections
                            in children.
                        </li>
                        <li>
                            It is however possible to have healthy HIV negative children
                            from HIV positive mothers. HIV/AIDS does not prevent
                            you from children.
                        </li>
                        <li>
                            Without treatment, there is a 15 - 45% chance of the
                            infection getting to your baby. With treatment, this
                            value goes below 5%.
                        </li>
                        <li>
                            Therefore there is the need on the part of mothers and
                            health care practitioners to know and implement the appropriate
                            information to reduce mother to child transmission (MTCT)
                            values to the minimum.
                        </li>
                    </ul>
                </li>
                <br>
                <li>
                    <h4 id="hiv-approach">Approach</h4>
                    <ol>
                        <li>
                            <h5>Step 1</h5>
                            <p>
                                At your booking visit (1st ANC visit) you will be
                                given a series of tests to do, including the HIV
                                test. You will be counseled prior to and after the
                                release of the test results.
                            </p>
                            <ul>
                                <li>
                                    If you test negative for HIV, it is important
                                    that you stay negative. If you have unprotected
                                    sex, and / or share needles and syringes, then
                                    test again.
                                </li>
                                <li>
                                    If you test positive for HIV, pay careful attention
                                    during your counseling about your health. You
                                    will be given a series of tests to do (pre-therapeutic
                                    work up) after which you should report to your
                                    doctor with the results.
                                </li>
                            </ul>
                        </li>
                        <p>Then move to Step 2.</p>
                        <li>
                            <h5>Step 2</h5>
                            <p>
                                International guidelines by the World Health Organization
                                (WHO) option B+ demand that you should be placed
                                on treatment as soon as you are diagnosed. You should
                                adhere to this treatment for life. You should take
                                the drugs at a particular time of the day at your
                                convenience and for the rest of your life. If complications
                                occur, please <span class="mln-note">SEE YOUR DOCTOR
                                </span> for assessment and maybe a change of the
                                drugs. <span class="mln-note">Do not stop taking the
                                drugs without informing your doctor</span>.
                            </p>
                            <p>
                                <span class="mln-note">NB:</span><br>
                                Benefits of Antiretrovirals:
                            </p>
                            <ul>
                                <li>Keep you strong and healthy</li>
                                <li>Make you less infectious</li>
                                <li>Protect your baby</li>
                            </ul>
                            <br>
                            <p>Things to avoid:</p>
                            <ul>
                                <li>Smoking</li>
                                <li>Alcohol</li>
                            </ul>
                        </li>
                        <li>
                            <h5>Step 3</h5>
                            <p>
                                When you get into labour at the hospital, inform
                                the doctor or midwife about your HIV status so that
                                dispositions can be taken to minimise chances of
                                transmission to your baby.<br>
                                You can deliver vaginally, as being HIV positive
                                does not prevent this. However, caesarean delivery
                                will be the fastest mode of delivery for you.
                            </p>
                            <p>
                                The decision on the route of delivery will be made
                                based on the situations surrounding delivery, your
                                doctor's opinion and your consent.
                            </p>
                        </li>
                        <li>
                            <h5>Step 4</h5>
                            <ul>
                                <li>
                                    <p>
                                        Feeding method depends on availability of
                                        resources.
                                    </p>
                                    <ul>
                                        <li>
                                            If the resources are available, bottle
                                            feeding is the gold standard. Seek appropriate
                                            counseling on how this is done, and hygiene
                                            should be prioritized.
                                        </li>
                                        <li>
                                            However, if the resources are not available,
                                            exclusive breastfeeding for 6 months
                                            should be done.
                                        </li>
                                        <li>
                                            <span class="mln-note">
                                                Mixed feeding should be avoided.
                                            </span>
                                        </li>
                                    </ul>
                                </li>
                                <br>
                                <li>
                                    <ul>
                                        <li>
                                            <span class="mln-note">If you decide to
                                             breastfeed</span>, your child should
                                             be given Nevirapine once a day from
                                             birth until 1 week after you stop breastfeeding.
                                        </li>
                                        <li>
                                            <span class="mln-note">If you decide
                                            not to breastfeed</span>, your child
                                            should receive Nevirapine once a day
                                            from birth until 6 weeks of age.
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <h5>Step 5</h5>
                            <p>
                                The age at which your baby can be tested for HIV
                                depends on the kind of test done and the method of
                                feeding.
                            </p>
                            <ul>
                                <li>
                                    <h5>PCR TEST</h5>
                                    <p>
                                        <span class="mln-note">If the baby is breastfeeding:</span><br>
                                        PCR test can be done from age 1 month and
                                        repeated 3 months after the child stops breastfeeding
                                        to ensure the child is negative.<br>
                                        <span class="mln-note">If the child is not
                                        breastfeeding:</span><br>
                                        PCR test can be done from age 1 month and
                                        repeated when the child is 4 months old to
                                        confirm the child is negative.<br>
                                        <span class="mln-note">If the child is on
                                        Nevirapine:</span><br>
                                        PCR test can be done from age 1 month and
                                        should be repeated 2 - 4 weeks after Nevirapine
                                        treatment is stopped.
                                    </p>
                                </li>
                                <li>
                                    <h5>ANTIBODY TEST</h5>
                                    <p>
                                        This test should be done when the child is
                                        at least 18 months old.
                                    </p>
                                    <ul>
                                        <li>
                                            If the child tests negative, he / she
                                            is negative.
                                        </li>
                                        <li>
                                            If the child tests positive, he / she
                                            is positive.
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <h5>Step 6</h5>
                            <ul>
                                <li>
                                    If your baby tests negative, it is important that
                                    your baby stays that way.
                                </li>
                                <li>
                                    If your baby tests positive, he / she should
                                    be placed on treatment as soon as possible.
                                </li>
                            </ul>
                        </li>
                    </ol>
                </li>
            </ul>
        </div>      
        <div class="ml-bb-section">
            <h3 id="hep">Hepatitis & Pregnancy</h3>
            <ul>
                <li>
                    <h4 id="hep-intro">Introduction</h4>
                    <ul>
                        <li>
                            Hepatitis B is the inflammation of the liver as a result
                            of infection from the hepatitis B virus. It may or may
                            not present itself with symptoms such as fever, loss
                            of appetite, vomiting, diarrhoea, yellowing of the eyes,
                            tiredness, body pains, dark urine and pale-coloured stool.
                        </li>
                        <li>
                            This virus can be transmitted through contact with infected
                            blood, semen, vaginal secretions and, to a lesser extend,
                            saliva.
                        </li>
                        <li>
                            Your immune system can protect you from hepatitis B infection
                            within a few months even without treatment. However,
                            5% of people continue carrying the virus. This leads
                            to damages to the liver in the long run.
                        </li>
                        <li>
                            10 - 20% of women who are Hepatitis B surface antigen
                            (HBsAg) positive will transmit the infection to their
                            babies without appropriate treatment of the mother and
                            child.
                        </li>
                    </ul>
                </li>
                <br>
                <li>
                    <h4 id="hep-approach">Approach</h4>
                    <ol>
                        <li>
                            <h5>Step 1</h5>
                            <p>
                                At your booking visit (1st ANC visit), among the
                                tests requested to be done on you will be Hepatitis
                                B surface antigen (HBsAg).
                            </p>
                            <ul>
                                <li>
                                    If you test negative, it is important you stay
                                    that way.
                                </li>
                                <li>
                                    If you test positive, you will be directed to
                                    a doctor who will run a test to check for the
                                    level of the virus in your blood. This is called
                                    the viral load.
                                </li>
                            </ul>
                        </li>
                        <li>
                            <h5>Step 2</h5>
                            <ul>
                                <li>
                                    If the viral load is low, your doctor will surely
                                    not put you on treatment immediately.
                                </li>
                                <li>
                                    If the viral load is high, you may be offered
                                    treatment either with lamivudine or tenofovir.
                                    Treatment will often start in the 3rd trimester
                                    of pregnancy (i.e. last 3 months of your pregnancy)
                                    and continue to the 12th week after delivery,
                                    and could even extend beyond this date depending
                                    on your response to the treatment and the evaluation
                                    from your doctor.
                                </li>
                            </ul>
                            <p>
                                <span class="mln-note">NB:</span> If you have already
                                been diagnosed and placed on treatment prior to your
                                pregnancy, inform your doctor about your treatment
                                to ensure the safety of your baby.
                            </p>
                        </li>
                        <li>
                            <h5>Step 3</h5>
                            <ul>
                                <li>
                                    When you get into labour, inform the doctor or
                                    midwife of your hepatitis B status so that the
                                    necessary precautions can be taken to ensure
                                    your baby's safety.
                                </li>
                                <li>
                                    You can deliver vaginally, given that being hepatitis
                                    B positive doesn't prevent you doing so.
                                </li>
                            </ul>
                        </li>
                        <li>
                            <h5>Step 4</h5>
                            <span class="mln-note">Vaccination</span>
                            <ul>
                                <li>
                                    At birth, your baby should receive the Hepatitis
                                    B vaccine.
                                </li>
                                <li>
                                    This vaccine should be followed up by an injection
                                    with antibodies against Hepatitis B.
                                </li>
                                <li>
                                    Your baby will need another vaccine at ages 1
                                    month, 2 months and 1 year. A booster is recommended
                                    for these vaccines at the age of 5 years.
                                </li>
                            </ul>
                            <p class="mln-note">
                                * It is important for your baby to receive the vaccines
                                because 9 out of 10 babies who get infected from
                                their mothers go on to have chronic hepatitis.<br>
                                * When you go for routine vaccination according to
                                the Expanded Program for Immunization, inform the
                                nurse of your status, emphasizing that your child
                                is already receiving hepatitis vaccines.
                            </p>
                            <span class="mln-note">Feeding</span><br>
                            It is safe for you to breastfeed your baby as long as
                            he / she is receiving his / her vaccines.
                        </li>
                        <li>
                            <h5>Step 5</h5>
                            <p>
                                Your baby should have a blood test around his / her
                                1st birthday to check if all the viruses are gone.
                            </p>
                            <ul>
                                <li>
                                    If your baby tests negative, it is important that
                                    he / she stays that way.
                                </li>
                                <li>
                                    If your baby tests positive, you should see a
                                    doctor for a more detailed evaluation of your
                                    child.
                                </li>
                            </ul>
                        </li>
                    </ol>
                </li>
            </ul>
        </div>
    </div>
</div>
