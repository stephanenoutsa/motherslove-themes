<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

/**
 * Remove admin bar
 */
function ml_remove_admin_bar() {
//    if ( !current_user_can('administrator') && !is_admin() ) {
//        show_admin_bar(false);
//    }
    show_admin_bar(false);
}
add_action( 'init', 'ml_remove_admin_bar' );

/**
 * Change logo on login form
 */
function my_login_logo() { ?>
    <style type="text/css">
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/login-logo.png);
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

/**
 * Redirect non-admin users to front page after login
 */
$redirect_to = site_url();
$user = wp_get_current_user();
function my_login_redirect( $redirect_to, $user ) {
	//is there a user to check?
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'administrator', $user->roles ) ) {
			// redirect them to the default place
			return admin_url();
		} else {
			return $redirect_to;
		}
	} else {
		return $redirect_to;
	}
}

add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );


/**
 * Enqueue easydropdown script and stylesheet
 */
function ml_enqueue_easydropdown() {
    wp_register_script('easydropdown', get_stylesheet_directory_uri() . '/bower_components/easydropdown/src/jquery.easydropdown.js');
    wp_enqueue_script('easydropdown');
    wp_enqueue_style('easydropdown', get_stylesheet_directory_uri() . '/bower_components/easydropdown/themes/easydropdown.css');
}
add_action('wp_enqueue_scripts', 'ml_enqueue_easydropdown');

/**
 * Enqueue responsiveslides script and stylesheet
 */
function ml_enqueue_responsiveslides() {
    wp_register_script('responsiveslides', get_template_directory_uri() . '/bower_components/ResponsiveSlides/responsiveslides.min.js', array(), NULL);
    wp_enqueue_script('responsiveslides');
    wp_enqueue_style('responsiveslides', get_stylesheet_directory_uri() . '/bower_components/ResponsiveSlides/responsiveslides.css');
}
add_action('wp_enqueue_scripts', 'ml_enqueue_responsiveslides');
